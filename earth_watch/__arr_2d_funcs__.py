from skimage import io
import cv2
import os
import urllib.request
import numpy as np
import regionmask
import geopandas
import pyvista as pv
import logging
import earth_watch.__interpolation_funcs__ as interp_funcs
import earth_watch.renderer as renderer


class CoordinateRange:
    def __init__(self, lat_min=None, lat_max=None,
                 long_min=None, long_max=None):
        self.lat_min = lat_min
        self.lat_max = lat_max
        self.long_min = long_min
        self.long_max = long_max


def set_coordinate_range(ds_color, ds_mesh, lat_range, long_range, region_coordinate_range):

    if lat_range is None:
        lat_range = [None, None]
    if long_range is None:
        long_range = [None, None]

    return CoordinateRange(np.nanmax(np.array([ds_mesh["latitude"][0],
                                               ds_color["latitude"][0],
                                               lat_range[0],
                                               region_coordinate_range.lat_min],
                                              dtype=float)),
                           np.nanmin(np.array([ds_mesh["latitude"][-1],
                                               ds_color["latitude"][-1],
                                               lat_range[1],
                                               region_coordinate_range.lat_max],
                                              dtype=float)),
                           np.nanmax(np.array([ds_mesh["longitude"][0],
                                               ds_color["longitude"][0],
                                               long_range[0],
                                               region_coordinate_range.long_min],
                                              dtype=float)),
                           np.nanmin(np.array([ds_mesh["longitude"][-1],
                                               ds_color["longitude"][-1],
                                               long_range[1],
                                               region_coordinate_range.long_max],
                                              dtype=float))
                           )


def download_image(img_url):
    urllib.request.urlretrieve(img_url, os.path.basename(img_url))


def get_region_mask(region_name):
    df_world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))

    lon = np.arange(-180, 180, 0.1)
    lat = np.arange(-90, 90, 0.1)
    mask = regionmask.mask_geopandas(df_world, lon, lat)
    mask = mask.data

    iloc = np.where(df_world == region_name)[0]
    if iloc.size == 0:
        logging.warning('Given region name for mask is not a country/continent. Skipping masking')
        return mask, CoordinateRange()

    mask[~np.isin(mask, iloc)] = np.nan

    value_indices = np.where(~np.isnan(mask))
    coordinate_range = CoordinateRange(lat[np.min(value_indices[0])], lat[np.max(value_indices[0])],
                                                lon[np.min(value_indices[1])], lon[np.max(value_indices[1])])

    return mask, coordinate_range


def load_arr_2d(arr, arr_slice=0, min_zero=False):
    arr = arr[arr_slice]
    arr = arr.filled(fill_value=np.nan)
    arr = arr - np.nanmin(arr) if min_zero else arr
    return arr


def load_and_prep_arr(arr_ref, idx_date, ds, is_min_zero, coordinate_range, data_shape, cube_size,
                      is_interp=False, region_mask=None):

    arr = load_arr_2d(arr_ref, int(idx_date), is_min_zero)

    if not float(idx_date).is_integer():
        arr_next = load_arr_2d(arr_ref, int(idx_date + 1), is_min_zero)
        arr = interp_funcs.interpolate_between_slices(arr, arr_next, idx_date % 1)

    arr = crop_and_resize_arr(arr, coordinate_range, cube_size, data_shape, ds)

    if region_mask is not None:
        arr[np.isnan(region_mask)] = np.nan

    if is_interp:
        arr = interp_funcs.interpolate_nans(arr)

    return arr


def crop_and_resize_arr(arr, coordinate_range, cube_size, data_shape, ds=None):

    arr = get_world_part(arr, coordinate_range, ds)
    arr = cv2.resize(arr, (data_shape[1] // cube_size, data_shape[0] // cube_size),
                     interpolation=cv2.INTER_NEAREST).T
    return arr


def find_idx_nearest(arr, val):
    return (np.abs(arr - val)).argmin()


def get_world_part(arr, coordinate_range, ds=None, padding=0):

    if ds is None:
        lat_arr_dst = np.linspace(-90, 90, arr.shape[0])
        long_arr_dst = np.linspace(-180, 180, arr.shape[1])
    else:
        lat_arr_dst = ds["latitude"][:]
        long_arr_dst = ds["longitude"][:]

    min_lat_idx = find_idx_nearest(lat_arr_dst, coordinate_range.lat_min)
    max_lat_idx = find_idx_nearest(lat_arr_dst, coordinate_range.lat_max)
    min_long_idx = find_idx_nearest(long_arr_dst, coordinate_range.long_min)
    max_long_idx = find_idx_nearest(long_arr_dst, coordinate_range.long_max)

    arr = arr[min_lat_idx - padding:max_lat_idx + padding, min_long_idx - padding:max_long_idx + padding]

    return arr


def get_bg_image(bg_padding, coordinate_range):
    download_image(renderer.RuntimeParameters.URL_BG.value)
    bg = get_world_part(np.flip(io.imread(os.path.basename(renderer.RuntimeParameters.URL_BG.value)), axis=0),
                        coordinate_range, padding=bg_padding)
    return bg


def get_hi_logo():
    download_image(renderer.RuntimeParameters.URL_HI_LOGO.value)
    hi_logo = pv.read(os.path.basename(renderer.RuntimeParameters.URL_HI_LOGO.value))
    return hi_logo


def get_bg_coordinate_arrays(bg_padding, coordinate_range, data_shape):
    lat_arr = np.linspace(coordinate_range.lat_min, coordinate_range.lat_max, data_shape[0])
    long_arr = np.linspace(coordinate_range.long_min, coordinate_range.long_max, data_shape[1])
    lat_arr_step = lat_arr[1] - lat_arr[0]
    long_arr_step = long_arr[1] - long_arr[0]
    lat_arr_bg = np.concatenate([np.flip(lat_arr[0] - lat_arr_step - np.arange(bg_padding) * lat_arr_step),
                                 lat_arr,
                                 lat_arr[-1] + lat_arr_step + np.arange(bg_padding) * lat_arr_step])
    long_arr_bg = np.concatenate([np.flip(long_arr[0] - long_arr_step - np.arange(bg_padding) * long_arr_step),
                                  long_arr,
                                  long_arr[-1] + long_arr_step + np.arange(bg_padding) * long_arr_step])
    return lat_arr_bg, long_arr_bg


def get_2d_data_arrays(arr_color_ref, arr_mesh_ref, coordinate_range, cube_size, data_shape, ds_color, ds_mesh,
                       i_date, is_interp, mesh_dates_indices_in_color, region_mask):

    arr_mesh = load_and_prep_arr(arr_mesh_ref, i_date, ds_mesh, True,
                                 coordinate_range, data_shape, cube_size, region_mask=region_mask)
    arr_color = load_and_prep_arr(arr_color_ref, mesh_dates_indices_in_color[i_date], ds_color, False,
                                  coordinate_range, data_shape, cube_size, is_interp == 'spatial')

    return arr_mesh, arr_color
