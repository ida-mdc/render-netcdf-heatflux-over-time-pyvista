import logging
import netCDF4 as nc


def load_nc(path_input):
    ds = nc.Dataset(path_input)
    arr_name = [n for n in ds.variables.keys() if ds[n].ndim == 3][0]
    arr_ref = ds[arr_name]
    logging.info(f'Read netCDF {arr_name} data. Shape={arr_ref.shape}')
    return ds, arr_ref
