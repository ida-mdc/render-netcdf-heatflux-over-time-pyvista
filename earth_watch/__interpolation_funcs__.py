import logging
import netCDF4 as nc
import numpy as np
from scipy.interpolate import griddata
from scipy.spatial import QhullError
import earth_watch.__nc_funcs__ as nc_funcs
import earth_watch.__arr_2d_funcs__ as arr_2d_funcs
import os


def save_nc_with_interp(arr_interp, ds, path_color_interp):
    var_names = list(ds.variables.keys())
    arr_name = [n for n in ds.variables.keys() if ds[n].ndim == 3][0]

    nc_file = nc.Dataset(path_color_interp, mode='w', format='NETCDF4_CLASSIC')

    # Define two variables with the same names as dimensions,
    # a conventional way to define "coordinate variables".
    time = nc_file.createVariable(var_names[0], np.float64, (var_names[0],))
    time.units = 'day as %Y%m%d.%f'
    time.long_name = var_names[0]

    lon = nc_file.createVariable(var_names[1], np.float32, (var_names[1],))
    lon.units = 'degrees_east'
    lon.long_name = var_names[1]

    lat = nc_file.createVariable(var_names[2], np.float32, (var_names[2],))
    lat.units = 'degrees_north'
    lat.long_name = var_names[2]

    # Define a 3D variable to hold the data
    data = nc_file.createVariable(var_names[3], np.float32,
                                  (var_names[0], var_names[2], var_names[1]))
    # note: unlimited dimension is leftmost
    data.units = 'W m-2'
    data.standard_name = arr_name

    nc_file.time_coverage_start = ds.time_coverage_start

    # fill in data
    time[:] = ds[var_names[0]][:]
    lon[:] = ds[var_names[1]][:]
    lat[:] = ds[var_names[2]][:]
    data[:] = arr_interp

    nc_file.close()
    logging.info('Saved netCDF Dataset with interpolation!')


def interpolate_nans(arr_color):
    if ~np.all(np.isnan(arr_color)):

        idxs_to_interp = np.where(np.isnan(arr_color))
        idxs_interp_from = np.where(~np.isnan(arr_color))

        try:
            arr_color[idxs_to_interp] = griddata(idxs_interp_from,
                                                 arr_color[idxs_interp_from],
                                                 idxs_to_interp)
        except QhullError:
            logging.warning('not interpolating - QHull error')

    return arr_color


def save_interpolated_nans_temporal(arr_color_ref, ds_color, path_color_interp):
    arr_interp = np.full(arr_color_ref.shape, np.nan, dtype=np.float16)

    for i in range(arr_color_ref.shape[1]):
        for j in range(arr_color_ref.shape[2]):
            arr_color = arr_2d_funcs.load_arr_2d(arr_color_ref, np.index_exp[:, i, j])
            arr_color = interpolate_nans(arr_color.astype(np.float16))

            arr_interp[:, i, j] = arr_color

    save_nc_with_interp(arr_interp, ds_color, path_color_interp)


def load_temporal_interpolation(arr_color_ref, ds_color, path_input_color):
    path_color_interp = f'{os.path.splitext(path_input_color)[0]}_temporalInterpolation.nc'
    if not os.path.exists(path_color_interp):
        save_interpolated_nans_temporal(arr_color_ref, ds_color, path_color_interp)
    ds_color, arr_color_ref = nc_funcs.load_nc(path_color_interp)
    return arr_color_ref, ds_color


def interpolate_between_slices(arr, arr_next, weight_next):
    interp_arr = arr * (1 - weight_next) + arr_next * weight_next
    return interp_arr
