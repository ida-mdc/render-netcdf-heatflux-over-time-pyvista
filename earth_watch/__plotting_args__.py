class PlotterArgs:
    def __init__(self):
        self.off_screen = True
        self.window_size = [1920, 1088]
        self.multi_samples = 8
        self.lighting = 'three lights'

    def to_dict(self):
        return self.__dict__


class MeshArgs:
    def __init__(self, cmap, clim):
        self.scalars = "colors"
        self.cmap = cmap
        self.clim = clim
        self.scalar_bar_args = {'vertical': True,
                                'position_x': 0.03,
                                'title': '',
                                'label_font_size': 20,
                                'width': 0.05}
        self.show_edges = True
        self.lighting = True
        self.name = 'mesh'

    def to_dict(self):
        return self.__dict__


class TextArgs:
    def __init__(self, height, str1, str2=None, name=None):
        self.text = self.__get_text__(str1, str2)
        self.position = (30, height)
        self.font_size = 14
        self.color = 'white'
        self.shadow = True
        self.name = name

    def to_dict(self):
        return self.__dict__

    def __get_text__(self, str1, str2=None):
        str1 = str1.replace("_", " ").upper()
        if len(str1) > 14 and str1.count(' ') > 2:
            temp = str1.split(' ')
            str1 = f"{' '.join(temp[:2])}\n{' '.join(temp[2:])}"
        if str2 is None:
            txt = str1
        else:
            str2 = str2.replace('_', " ").upper()
            nl = '\n'
            txt = f'Height:{nl}{str1}{nl}{nl}{nl}Color:{nl}{str2}'

        return txt


class EventArgs:
    def __init__(self):
        self.name = 'event'
        self.italic = True
        self.font_size = 30
        self.shape_color = 'black'
        self.point_color = 'gray'
        self.point_size = 40
        self.shape_opacity = 0.5
        self.render_points_as_spheres = True
        self.always_visible = True
        self.shadow = True

    def to_dict(self):
        return self.__dict__


class CubeArgs:
    def __init__(self, bg):
        floor_thickness = 20
        self.center = (bg.center[0], bg.center[1], - floor_thickness)
        self.x_length = bg.bounds[1] - bg.bounds[0]
        self.y_length = bg.bounds[3] - bg.bounds[2]
        self.z_length = floor_thickness * 2 - 0.02

    def to_dict(self):
        return self.__dict__


def init_set_camera(p):
    p.camera_position = 'xy'
    p.camera.zoom(2)
    p.camera.azimuth += 20
    p.camera.elevation = -15
    return p
