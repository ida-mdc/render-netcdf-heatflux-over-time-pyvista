import pyvista as pv
import logging
from enum import Enum

import earth_watch.__nc_funcs__ as nc_funcs
import earth_watch.__plotting_args__ as p_args
import earth_watch.__plotting_funcs__ as p_funcs
import earth_watch.__time_funcs__ as time_funcs
import earth_watch.__events_funcs__ as events_funcs
import earth_watch.__create_mesh_funcs__ as create_mesh
import earth_watch.__interpolation_funcs__ as interp_funcs
import earth_watch.__arr_2d_funcs__ as arr_2d_funcs

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class RuntimeParameters(Enum):
    N_INTERPOLATIONS = 10
    URL_BG = "https://gitlab.com/ida-mdc/earth-watch/-/raw/main/earth_watch/bluemarble.png"
    URL_HI_LOGO = "https://gitlab.com/ida-mdc/earth-watch/-/raw/main/earth_watch/HI_logo.png"
    AZIMUTH = 0.02
    CUBE_MAX_HEIGHT = 500


def run_render(path_input_mesh, path_input_color, clim, path_output="output.mp4",
               n_timepoints=None, region_name=None, lat_range=None, long_range=None, is_interp=None,
               cube_size=20, cmap='PiYG', path_event_csv=None, title=None, bg_padding=300,
               cube_height_multiplier=1, frame_rate=30, logo_path=None):

    ds_mesh, arr_mesh_ref = nc_funcs.load_nc(path_input_mesh)
    ds_color, arr_color_ref = nc_funcs.load_nc(path_input_color)

    if is_interp == 'temporal':
        arr_color_ref, ds_color = interp_funcs.load_temporal_interpolation(arr_color_ref,
                                                                           ds_color,
                                                                           path_input_color)

    dates_mesh, mesh_dates_idxs_in_color = time_funcs.format_dates_arrays(ds_color, ds_mesh)

    region_mask, region_coordinate_range = arr_2d_funcs.get_region_mask(region_name)

    coordinate_range = arr_2d_funcs.set_coordinate_range(ds_color, ds_mesh, lat_range, long_range,
                                                         region_coordinate_range)

    bg = arr_2d_funcs.get_bg_image(bg_padding, coordinate_range)
    bg_mesh = create_mesh.make_bg_mesh(bg, bg_padding)

    data_shape = [s - bg_padding * 2 for s in bg.shape[:2]]

    region_mask = arr_2d_funcs.crop_and_resize_arr(region_mask, coordinate_range, cube_size, data_shape)

    events_per_time_idx_dict = {}
    if path_event_csv is not None:
        events_per_time_idx_dict = events_funcs.get_events(path_event_csv,
                                                           dates_mesh,
                                                           data_shape,
                                                           coordinate_range,
                                                           bg_padding,
                                                           cube_height_multiplier)

    p = p_funcs.create_plotter(path_output, frame_rate)
    p = p_funcs.add_bg(p, bg_mesh)

    logo_hi = arr_2d_funcs.get_hi_logo()
    logo_widget_hi = p_funcs.add_logo(p, logo_hi, position=(.86, -.02))
    if logo_path is not None:
        logo_user = pv.read(logo_path)
        logo_widget_user = p_funcs.add_logo(p, logo_user, position=(.86, 0.875))

    if title is not None: p.add_text(**p_args.TextArgs(1000, title).to_dict())
    p.add_text(**p_args.TextArgs(750, arr_mesh_ref.name, arr_color_ref.name).to_dict())

    if n_timepoints is None:
        n_timepoints = ds_mesh["time"].size

    for i_date in range(n_timepoints - 1):

        arr_mesh_curr, arr_color_curr = arr_2d_funcs.get_2d_data_arrays(arr_color_ref, arr_mesh_ref,
                                                                        coordinate_range, cube_size, data_shape,
                                                                        ds_color, ds_mesh, i_date, is_interp,
                                                                        mesh_dates_idxs_in_color, region_mask
                                                                        ) if i_date == 0 else [arr_mesh_next,
                                                                                               arr_color_next]

        arr_mesh_next, arr_color_next = arr_2d_funcs.get_2d_data_arrays(arr_color_ref, arr_mesh_ref,
                                                                        coordinate_range, cube_size, data_shape,
                                                                        ds_color, ds_mesh, i_date+1, is_interp,
                                                                        mesh_dates_idxs_in_color, region_mask)

        for i_interp in range(RuntimeParameters.N_INTERPOLATIONS.value + 1):

            p_funcs.create_scene(arr_color_curr, arr_color_next, arr_mesh_curr, arr_mesh_next,
                                 clim, cmap, cube_size, cube_height_multiplier,
                                 dates_mesh, events_per_time_idx_dict, i_date, i_interp, p)

    pv.close_all()

