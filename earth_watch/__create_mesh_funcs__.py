import numpy as np
from skimage import io
import pyvista as pv
import earth_watch.renderer as renderer


def make_floor_vertices(arr, cube_size):
    xs, ys = np.meshgrid(range(arr.shape[0]), range(arr.shape[1]))

    vertices_base = np.vstack((xs.T.flatten() * cube_size,
                               ys.T.flatten() * cube_size,
                               np.zeros(xs.size))).T

    xs, ys = np.meshgrid(range(arr.shape[0]), [arr.shape[1]])
    vertices_edge_y = np.vstack((xs.T.flatten() * cube_size,
                                 ys.T.flatten() * cube_size,
                                 np.zeros(xs.size))).T

    xs, ys = np.meshgrid(arr.shape[0], range(arr.shape[1]+1))
    vertices_edge_x = np.vstack((xs.T.flatten() * cube_size,
                                 ys.T.flatten() * cube_size,
                                 np.zeros(xs.size))).T

    return vertices_base, vertices_edge_y, vertices_edge_x


def concat_all_vertices(arr, vertices_base, vertices_edge_y, vertices_edge_x, cube_size, cube_height_multiplier):
    vertices_ceil = vertices_base.copy()

    arr_pwr = np.power(arr.flatten(), 2)
    arr_normed = (arr_pwr - np.nanmin(arr_pwr)) / (np.nanmax(arr_pwr) - np.nanmin(arr_pwr))
    vertices_ceil[:, 2] = arr_normed * renderer.RuntimeParameters.CUBE_MAX_HEIGHT.value * cube_height_multiplier

    vertices = np.concatenate((vertices_base,
                               vertices_edge_y,
                               vertices_edge_x,
                               vertices_ceil,
                               vertices_ceil + [0, 1 * cube_size, 0],
                               vertices_ceil + [1 * cube_size, 1 * cube_size, 0],
                               vertices_ceil + [1 * cube_size, 0, 0],
                               ))

    return vertices


def make_vertices(arr, cube_size, cube_height_multiplier):
    vertices_base, vertices_edge_y, vertices_edge_x = make_floor_vertices(arr, cube_size)
    vertices = concat_all_vertices(arr, vertices_base, vertices_edge_y, vertices_edge_x,
                                   cube_size, cube_height_multiplier)

    n_floor_v = vertices_base.shape[0] + vertices_edge_y.shape[0] + vertices_edge_x.shape[0]
    return vertices, n_floor_v


def make_floor_faces(arr):
    faces = []
    it = np.nditer(arr, flags=['c_index', 'multi_index'])

    for x in it:
        if not np.isnan(x):

            neigh_right_idx = it.index + 1 if it.multi_index[1] != arr.shape[1] - 1 \
                else arr.size + it.multi_index[0]
            neigh_down_idx = it.index + arr.shape[1] if it.multi_index[0] != arr.shape[0] - 1 \
                else arr.size + arr.shape[0] + it.multi_index[1]

            neigh_diagonal_idx = it.index + arr.shape[1] + 1
            if neigh_down_idx > arr.size:
                neigh_diagonal_idx = neigh_down_idx + 1
            elif neigh_right_idx > arr.size - 1:
                neigh_diagonal_idx = neigh_right_idx + 1

            faces.append(np.array([4,
                                   it.index,
                                   neigh_right_idx,
                                   neigh_diagonal_idx,
                                   neigh_down_idx,
                                   ]))

    return faces


def make_ceil_faces(arr, n_v_floor):
    faces = []
    it = np.nditer(arr, flags=['c_index', 'multi_index'])

    for x in it:
        if not np.isnan(x):
            faces.append(np.array([4,
                                   n_v_floor + it.index,
                                   n_v_floor + arr.size + it.index,
                                   n_v_floor + arr.size * 2 + it.index,
                                   n_v_floor + arr.size * 3 + it.index,
                                   ]))

    return faces


def make_side_faces(faces):
    side_faces = []

    for i in range(len(faces) // 2):
        side_faces.append(np.array([4,
                                    faces[i][1],
                                    faces[i][2],
                                    faces[len(faces) // 2 + i][2],
                                    faces[len(faces) // 2 + i][1],
                                    ]))
        side_faces.append(np.array([4,
                                    faces[i][1],
                                    faces[i][4],
                                    faces[len(faces) // 2 + i][4],
                                    faces[len(faces) // 2 + i][1],
                                    ]))
        side_faces.append(np.array([4,
                                    faces[i][2],
                                    faces[i][3],
                                    faces[len(faces) // 2 + i][3],
                                    faces[len(faces) // 2 + i][2],
                                    ]))
        side_faces.append(np.array([4,
                                    faces[i][3],
                                    faces[i][4],
                                    faces[len(faces) // 2 + i][4],
                                    faces[len(faces) // 2 + i][3],
                                    ]))

    return side_faces


def make_faces(arr, n_floor_v):
    faces = make_floor_faces(arr)
    faces.extend(make_ceil_faces(arr, n_floor_v))
    faces.extend(make_side_faces(faces))

    return faces


def make_face_color(arr_color_flat, arr_mesh):
    arr_color_flat = np.delete(arr_color_flat, np.isnan(arr_mesh.flatten(order='F')))

    faces_color = np.concatenate((np.tile(arr_color_flat, 2),
                                  np.tile(arr_color_flat, (4, 1)).flatten(order='F')))

    return faces_color


def make_surface(arr_mesh, cube_size, cube_height_multiplier, arr_color):
    vertices, n_floor_v = make_vertices(arr_mesh, cube_size, cube_height_multiplier)
    faces = make_faces(arr_mesh, n_floor_v)
    surf = pv.PolyData(vertices, faces)
    faces_color = make_face_color(arr_color.flatten(order='F'), arr_mesh)
    surf["colors"] = faces_color

    return surf


def make_bg_mesh(bg, bg_padding):
    path_bg = 'tmp.png'
    io.imsave(path_bg, np.flip(bg, axis=0))
    bg_mesh = pv.read(path_bg)
    bg_mesh.origin = (-bg_padding, -bg_padding, 0)
    return bg_mesh
