import pyvista as pv
import vtk
import earth_watch.renderer as renderer
import earth_watch.__plotting_args__ as p_args
import earth_watch.__interpolation_funcs__ as interp_funcs
import earth_watch.__create_mesh_funcs__ as create_mesh
import earth_watch.__time_funcs__ as time_funcs
import earth_watch.__arr_2d_funcs__ as arr_2d_funcs


def create_plotter(path_output, frame_rate):
    p = pv.Plotter(**p_args.PlotterArgs().to_dict())
    p.open_movie(path_output, framerate=frame_rate)
    p.set_background('black')
    return p


def add_bg(p, bg):
    p.add_mesh(pv.Cube(**p_args.CubeArgs(bg).to_dict()), color='white')
    p.add_mesh(bg, rgb=True, scalars='PNGImage')

    return p


def add_logo(p, image, position):
    logo_representation = vtk.vtkLogoRepresentation()
    logo_representation.SetImage(image)
    logo_representation.SetPosition(*position)  # x,y
    logo_representation.SetPosition2(.12, .12)  # size
    logo_representation.GetImageProperty().SetOpacity(1.0)
    logo_widget = vtk.vtkLogoWidget()
    logo_widget.SetInteractor(p.iren.interactor)
    logo_widget.SetRepresentation(logo_representation)
    logo_widget.On()
    p.logo_widget = logo_widget  # have to keep a reference to the widget
    return logo_widget


def create_scene(arr_color_curr, arr_color_next, arr_mesh_curr, arr_mesh_next,
                 clim, cmap, cube_size, cube_height_multiplier,
                 dates_mesh, events_per_time_idx_dict, i_date, i_interp, p):

    p = remove_actors(p)
    
    arr_mesh = interp_funcs.interpolate_between_slices(arr_mesh_curr, arr_mesh_next,
                                                       i_interp / renderer.RuntimeParameters.N_INTERPOLATIONS.value)
    arr_color = interp_funcs.interpolate_between_slices(arr_color_curr, arr_color_next,
                                                        i_interp / renderer.RuntimeParameters.N_INTERPOLATIONS.value)
    
    surf = create_mesh.make_surface(arr_mesh, cube_size, cube_height_multiplier, arr_color)
    
    p.add_mesh(surf, **p_args.MeshArgs(cmap, clim).to_dict())
    p.add_text(**p_args.TextArgs(600,
                                 time_funcs.format_date_for_legend(dates_mesh[i_date]),
                                 name='date').to_dict())
    
    if str(i_date) in events_per_time_idx_dict:
        p.add_point_labels(events_per_time_idx_dict[str(i_date)][0],
                           events_per_time_idx_dict[str(i_date)][1],
                           **p_args.EventArgs().to_dict())
        
    if i_date == 0 and i_interp == 0: 
        p = p_args.init_set_camera(p)
        
    p.camera.azimuth -= renderer.RuntimeParameters.AZIMUTH.value
    p.write_frame()


def remove_actors(p):
    p.remove_actor('mesh')
    p.remove_actor('event')
    p.remove_actor('date')
    return p
