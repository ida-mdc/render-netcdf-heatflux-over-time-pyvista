import pandas as pd
import logging
import numpy as np
import earth_watch.__arr_2d_funcs__ as arr_2d_funcs


def get_events(path_event_csv, time_arr, data_shape, coordinate_range, bg_padding, cube_height_multiplier):

    lat_arr, long_arr = arr_2d_funcs.get_bg_coordinate_arrays(bg_padding, coordinate_range, data_shape)

    event_dict = {}

    df = pd.read_csv(path_event_csv, dtype={"first_date": str, "last_date": str})
    for i in df.index:
        try:
            idx_first_date = np.where(time_arr == int(df.loc[i, "first_date"]))[0][0]
            idx_last_date = np.where(time_arr == int(df.loc[i, "last_date"]))[0][0]
        except Exception as e:
            logging.exception(e)
            logging.warning(f'event csv has an invalid date - row #{i}')
            continue

        if (not long_arr[0] <= df.loc[i, "longitude"] <= long_arr[-1]) or (
                not lat_arr[0] <= df.loc[i, "latitude"] <= lat_arr[-1]):
            logging.warning(f'event csv has an invalid location - row #{i}')
            continue

        point = [(np.abs(long_arr - df.loc[i, "longitude"])).argmin() - bg_padding,  # x
                 (np.abs(lat_arr - df.loc[i, "latitude"])).argmin() - bg_padding,  # y
                 0]  # z

        text = (df.loc[i, "text"]).replace('\\n', '\n')

        for idx in range(idx_first_date, idx_last_date + 1):

            if str(idx) not in event_dict:
                event_dict[str(idx)] = [[point], [text]]
            else:
                event_dict[str(idx)][0].append(point)
                event_dict[str(idx)][1].append(text)

    return event_dict
