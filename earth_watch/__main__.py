import earth_watch.renderer as renderer
import argparse
from pathlib import Path

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument('--path_input_mesh', required=True, type=Path)
    parser.add_argument('--path_input_color', required=True, type=Path)
    parser.add_argument('--clim', required=True, nargs='+', type=float)
    parser.add_argument('--path_output', default='output.mp4', type=Path)
    parser.add_argument('--n_timepoints', default=None, type=int)
    parser.add_argument('--region_name', default=None)
    parser.add_argument('--lat_range', default=None, nargs='+', type=float)
    parser.add_argument('--long_range', default=None, nargs='+', type=float)
    parser.add_argument('--is_interp', default=None)
    parser.add_argument('--cube_size', default=20, type=int)
    parser.add_argument('--cmap', default='PiYG')
    parser.add_argument('--path_event_csv', default=None, type=Path)
    parser.add_argument('--title', default=None)
    parser.add_argument('--bg_padding', default=300, type=int)
    parser.add_argument('--cube_height_multiplier', default=1, type=float)
    parser.add_argument('--frame_rate', default=30, type=int)
    parser.add_argument('--logo_path', default=None, type=Path)

    args = parser.parse_args()

    renderer.run_render(args.path_input_mesh,
                        args.path_input_color,
                        args.clim,
                        args.path_output,
                        args.n_timepoints,
                        args.region_name,
                        args.lat_range,
                        args.long_range,
                        args.is_interp,
                        args.cube_size,
                        args.cmap,
                        args.path_event_csv,
                        args.title,
                        args.bg_padding,
                        args.cube_height_multiplier,
                        args.frame_rate,
                        args.logo_path)
