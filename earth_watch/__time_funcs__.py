from datetime import datetime, timedelta
import dateutil.parser
import numpy as np
import logging


def format_arr_Ymd(ds):
    date_arr = ds['time'][:].astype(int)

    if isinstance(date_arr, np.ma.masked_array):
        date_arr = date_arr.filled(fill_value=np.nan)

    first_val = date_arr[0]

    try:
        datetime.strptime(str(first_val), '%Y%m%d')
    except ValueError:
        logging.info('Time array format is not in e.g. 20201231 format - trying to parse.')
        try:
            ds.time_coverage_start
        except AttributeError:
            raise Exception("Can't parse netCDF dataset time format - " +
                            "needs either e.g. 20201231 format or " +
                            "ds.time_coverage_start to be set in nc file")

        start_date_obj = dateutil.parser.parse(ds.time_coverage_start)
        date_arr = [start_date_obj + timedelta(days=int(it - first_val)) for it in date_arr]
        date_arr = np.array([int(it.strftime("%Y%m%d")) for it in date_arr])

    return date_arr


def format_date_for_legend(date):
    date_obj = datetime.strptime(str(date), '%Y%m%d')
    return date_obj.strftime("%b %d, %Y")


def get_days_delta(date1_int, date2_int):
    return (datetime.strptime(str(date1_int), '%Y%m%d') - datetime.strptime(str(date2_int), '%Y%m%d')).days


def map_mesh_dates_to_color_idx(dates_mesh, dates_color):
    closest_smaller_idx_in_color = [np.where((dates_color - d) <= 0)[0][-1] for d in dates_mesh]
    distance_in_days_from_smaller = np.array(
        [get_days_delta(d, dates_color[closest_smaller_idx_in_color[i]]) for i, d in enumerate(dates_mesh)])
    distance_in_days_from_larger = np.array([get_days_delta(dates_color[closest_smaller_idx_in_color[i] + 1], d)
                                             if dates_color.size > closest_smaller_idx_in_color[i] + 1 else np.inf
                                             for i, d in enumerate(dates_mesh)])

    relative_distance_from_smaller = distance_in_days_from_smaller / (
            distance_in_days_from_smaller + distance_in_days_from_larger)

    indices_as_float = np.array(closest_smaller_idx_in_color) + relative_distance_from_smaller
    return indices_as_float


def format_dates_arrays(ds_color, ds_mesh):
    dates_mesh = format_arr_Ymd(ds_mesh)
    dates_color = format_arr_Ymd(ds_color)
    mesh_dates_indices_in_color = map_mesh_dates_to_color_idx(dates_mesh, dates_color)
    return dates_mesh, mesh_dates_indices_in_color
