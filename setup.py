from distutils.core import setup

setup(name='earth_watch',
version='0.1.0',
description='render netCDF data over time-points',
author='Ella Bahry',
author_email='ella.bahry@mdc-berlin.de',
url="https://gitlab.com/ida-mdc/earth-watch",
packages=['earth_watch'],
)
