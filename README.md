# earth-watch  

Python package for rendering netCDF map over time as a video using PyVista.

### Example output  
![example1](assets/example1.gif)
![example2](assets/example2.gif)  
*Snapshots taken from videos created by the tool - made by Almudena Garcia-Garcia and Jian Peng of Umweltforschungszentrum (UFZ) - García-García et al., (In preparation)*

### Basic run:

Create a conda env using `env.yml`, activate the environment, and install the python package (e.g. clone the repo and run `setup.py`). Then run:  
`earth-watch --path_input_mesh <path/to/dataset.nc> --path_input_color <path/to/dataset.nc> --clim <min> <max>`

### Arguments:  

`--path_input_mesh, required=True, type=Path`   
Path to netCDF dataset to base the mesh shape on.  

`--path_input_color, required=True, type=Path`  
Path to netCDF dataset to base the mesh color on  

`--clim, required=True, type=tuple(float, float)`  
Color legend clim (min max) - format e.g. `0 100.`  

`--path_output, default='output.mp4', type=Path`  
Path to the output movie - should end with .mp4  

`--n_timepoints, default=None, type=int`  
Number of timepoints to be rendered (starting from first time point)  

`--region_name, default=None`   
Name of country or continent (capitalize first letter) to use as a region mask  

`--lat_range, default=None, type=tuple(float, float)`  
Crop values of latitude array - format e.g. `30. 50.`  

`--long_range, default=None, type=tuple(float, float)`  
Crop values of longitude array - format e.g. `30. 50.`  

`--is_interp, default=None`  
Should interpolation be done on input_color - valid values: `None`, `spatial`, `temporal`.  

`--cube_size, default=20, type=int`  
Number of pixels (nxn) to be averaged to one value in mesh/color array.  

`--cmap, default='PiYG'`  
Color map (i.e. matplotlib naming - use `_r` seffix for the revered)  

`--path_event_csv, default=None`  
csv with events to display on the map - example table below.

`--title, default=None`  
Title of the video.  

`--bg_padding, default=300, type=int`  
Number of pixels to pad the background world map.  

`--cube_height_multiplier, default=1, type=float`  
Change the height of the mesh cubes - 0-1 values for shorter cubes, heigher than 1 for bigger.  

`--frame_rate, default=30, type=int`  
Video speed as frame rate per second.  


#### Example event CSV:  

| first_date  |  last_date  |  longitude  |  latitude   |    text                      |
| ----------- | ----------- | ----------- | ----------- | ---------------------------- |
| 20010604    | 20010616    |    12.15    |     50      | lorem ipsum                  |
| 20010612    | 20010630    |    10.25    |     25      | Lorem ipsum \ndolor sit amet |


### Authors:  
Schmidt Lab of Helmholz Imaging in collaboration with Almudena Garcia-Garcia and Jian Peng of Umweltforschungszentrum (UFZ).
Data represented in the gifs above from García-García et al., (In preparation).
https://helmholtz-imaging.de/   
https://www.ufz.de/index.php?en=48750


### Rendering was done using the open-source python package PyVista:  
Sullivan and Kaszynski, (2019). PyVista: 3D plotting and mesh analysis through a streamlined interface for the Visualization Toolkit (VTK). Journal of Open Source Software, 4(37), 1450, https://doi.org/10.21105/joss.01450

### License:  

The MIT License  

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.  
